<?php
include 'dbconnection.php';

session_start();

$username = $_POST['username-f'];
$password = $_POST['password-f'];

$query = $conn->prepare('SELECT * FROM `users` WHERE username = :username');
$query->execute(array(
    ':username' => $username,


));


$result = $query->fetch(PDO::FETCH_ASSOC);
if ($query->rowCount() != 0 && password_verify($password, $result['password'])) {

    $_SESSION['login'] = true;
    $_SESSION['userid'] = $result['User_ID'];

    header("location: ../?page=home");
} else {
    $_SESSION['error'] = "verkeerde informatie ";
    header("Location: ../?page=login");
}

$_SESSION['role'] = $result['role']

?>

