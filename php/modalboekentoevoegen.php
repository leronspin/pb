<?php
include'DBconnection.php';


$boek = $_POST['boek-f'];
$auteur = $_POST['auteur-f'];
$uitgever = $_POST['uitgever-f'];
$file = $_POST['fileupload-f'];

$pdoQuery = "INSERT INTO boeken (naam,auteur,uitgever,cover) VALUES (:boek, :auteur, :uitgever, :cover)";

$query = $conn->prepare($pdoQuery);

$result = $query->fetch(PDO::FETCH_ASSOC);


$query->execute(array(
    ":boek"=>$boek,
    ":auteur"=>$auteur,
    ":uitgever"=>$uitgever,
    ":cover" =>$file

));

header('location: ../index.php?page=boekentoevoegen');