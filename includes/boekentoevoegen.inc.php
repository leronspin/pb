<?php
include 'php/dbconnection.php';


$query = $conn->prepare('SELECT naam, boek_ID, auteur, uitgever, cover FROM boeken');

$query->execute(array());

$result = $query->fetchAll(PDO::FETCH_ASSOC);


?>


<br>
<div class="container">
    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">
        Toevoegen
    </button>
    <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#exampleModal2">
        Verwijderen
    </button>
    <br>
    <br>
</div>


<!-- modal toevoegen -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Boek toevoegen</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="../php/modalboekentoevoegen.php" method="post" enctype="multipart/form-data">
                    <div class="form-group input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text"> <i class="fa fa-lock"></i> </span>
                        </div>
                        <input name="boek-f" class="form-control" placeholder="Boek naam" type="text">
                    </div>
                    <div class="form-group input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text"> <i class="fa fa-lock"></i> </span>
                        </div>
                        <input name="auteur-f" class="form-control" placeholder="Auteur" type="text">
                    </div>
                    <div class="form-group input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text"> <i class="fa fa-lock"></i> </span>
                        </div>
                        <input name="uitgever-f" class="form-control" placeholder="Uitgever" type="text">
                    </div>
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text" id="inputGroupFileAddon01">Cover</span>
                        </div>
                        <div class="custom-file">
                            <input type="file" name="fileupload-f" class="custom-file-input" id="inputGroupFile01"
                                   aria-describedby="inputGroupFileAddon01">
                            <label class="custom-file-label" for="inputGroupFile01">Choose file</label>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Toevoegen</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<br>

<!-- modal verwijderen -->
<div class="modal fade" id="exampleModal2" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Boek verwijderen</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="../php/modalboekenverwijderen.php" method="post">
                    <div class="form-group">
                        <select class="form-control" id="sel1" name="id">
                            <?php
                            foreach ($result as $results) { ?>

                                <option value="<?= $results['boek_ID'] ?>"><?= $results['naam'] ?></option>
                            <?php } ?>
                        </select>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-danger">Verwijderen</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<br>

<div class="container">
    <ul class="list-group">
        <?php
        foreach ($result as $results) { ?>
            <div class="container">
                <div class="row">
                    <div class="col-2 list-group-item">
                        <b>Naam: </b><?=$results['naam'] ?><br>
                        <b>Auteur: </b><?=$results['auteur'] ?><br>
                        <b>Uitgever: </b><?=$results['uitgever'] ?><br>
                    </div>
                    <div class="col-2 list-group-item" >
                        <img src="">
                    </div>
                </div>
            </div>
       <?php } ?>
    </ul>
</div>

<?php
echo '<img src="data:image/png;base64,' . $results['cover'] . '">';

?>